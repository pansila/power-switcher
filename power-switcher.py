from flask import Flask, url_for
from flask import render_template
from flask import request
import serial
import logging
from flask import jsonify
import json

app = Flask(__name__)
#ser = None
portnum = 28
#userdb = None
#dbfile = None
dbfilepath = "./database.json"

@app.route("/")
def hello(database=None):
	return render_template('index.html', database=userdb, portnum=portnum)

@app.route("/update", methods=['POST'])
def update():
	global userdb
	ret = request.get_json()
	userdb = ret
	with open(dbfilepath, "w") as dbfile:
		try:
			validate_data()
		except Exception as msg:
			app.logger.warning(msg)
		finally:
			json.dump(userdb, dbfile, indent=4, sort_keys=True)
	return jsonify(userdb)

@app.route("/load")
def loaddata():
	with open(dbfilepath, "r+") as dbfile:
		global userdb
		userdb = json.load(dbfile)
		try:
			validate_data()
		except Exception as msg:
			app.logger.warning(msg)
			dbfile.seek(0)
			json.dump(userdb, dbfile, indent=4, sort_keys=True)
			dbfile.truncate()
	return jsonify(userdb)

@app.route("/control", methods=['POST'])
def control():
	global ser
	command = request.get_json()
	if not ser is None:
		try:
			port = str(int(command['portnum']) - 1) # contorller accepts zero starting index
			status = "0" if command['status'] else "1"
			ser.write("#" + port + ":" + status + "\n")
		except Exception as msg:
			print(msg)
			app.logger.warning(msg)
	return jsonify(command)

def validate_data():
	""" Remove duplicate entries """
	global userdb
	allports = {}
	delports = {}
	for user in userdb:
		for port in userdb[user]:
			if port in allports.keys():
				delports[user] = port
			else:
				allports[port] = 1

	for user in delports:
		del userdb[user][delports[user]]
	if len(delports) != 0:
		raise Exception("conflicted config" + str(delports))

if __name__ == "__main__":
	global ser
	with open(dbfilepath, "r+") as dbfile:
		global userdb
		userdb = json.load(dbfile)
		try:
			validate_data()
		except Exception as msg:
			print msg
			dbfile.seek(0)
			json.dump(userdb, dbfile, indent=4, sort_keys=True)
			dbfile.truncate()
	try:
		ser = serial.Serial("/dev/ttyACM0", 9600, timeout=0, parity=serial.PARITY_NONE, rtscts=0)
		for u in userdb:
			for p in userdb[u]:
				port = str(int(p) - 1) # contorller accepts zero starting index
				ser.write("#" + port + ":" + str(userdb[u][p].get('state')) + "\n")
	except Exception as msg:
		ser = None
		print(msg)
		exit()
	app.debug = True
	app.run(
		debug=False,
		host="0.0.0.0",
		port=5000
	)
	try:
		ser.close()
	except Exception as msg:
		print(msg)
